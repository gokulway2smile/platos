<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\TestimonialModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Testimonial extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = TestimonialModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = TestimonialModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    

    public function store(Request $request)
    {
        // 
        $rules = [
            'title'    => 'required',
            'description' => 'required'        
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $testi                    = new TestimonialModel();
        $testi->testi_title       = $data['title'];
        $testi->testi_description = $data['description'];
        $testi->testi_username    = $data['username'];
        $testi->testi_image       = $data['image'];
        $testi->save();

        return Base::touser('Testimonial Created', true);
    
    }


    public function image_upload(Request $request)
    {
       
        if(!empty($request->file('pic')))
        {

        $image = $request->file('pic');

        $input['pic'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('images/testimonial/');
        $input['file_path'] = "images/testimonial/";
        $image->move($destinationPath, $input['pic']);
      
        }


       return Base::touser($input, true);
    }


    public function show(Request $request,$id)
    {
            

            $data = TestimonialModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'title'    => 'required',
            'description' => 'required'        
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $testi                    = new TestimonialModel();
        $testi                    = $testi->where('testi_id', '=', $id)->first();
        $testi->testi_title       = $data['title'];
        $testi->testi_description = $data['description'];
        $testi->testi_username    = $data['username'];
        $testi->testi_image    = $data['image'];    
                
        $testi->save();
        return Base::touser('Testimonial Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new TestimonialModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Testimonial Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Testimonial its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
