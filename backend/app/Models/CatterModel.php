<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class CatterModel extends Model 
{
    
    // public function cust()
    // {
    //     return $this->hasMany('App\Models\Customer', 'emp_id','user_id');
    // }


    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_caterer';


    protected $primaryKey = 'catt_id';

    public $timestamps = true;

    protected $hidden = array('catt_password');

    protected $dates = ['deleted_at'];



    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'catt_first_name',
        'catt_sur_name',
        'catt_email_address',
        'catt_mobile_no',
        'catt_image',
        'catt_delivery_pincode',
        'catt_delivery_time',
        'catt_min_order',
        'catt_max_order',
        'catt_lead_time',
        'catt_event_type',
        'catt_gst_certificate',
        'catt_food_certificate',
        'catt_score',
        'catt_file_upload',
        'catt_category'
                  
    ];

    protected $guarded = [];
}
