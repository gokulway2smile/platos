<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(array('prefix' => 'api'), function () {


Route::get('getall_catterlist', 'Catters@getall_list');

Route::get('getall_Userlist', 'Users@getall_list');

Route::get('get_Userdetails/{id}', 'Users@get_userdetails'); 

Route::resource('getall_testimonial', 'Testimonial');  

Route::post('web_login', 'Admin@Auth_weblogin');

Route::post('Catter_NewAdmin', 'Catters@Create_admin');


// Testimonial 

Route::post('testi_image', 'Testimonial@image_upload');



});



