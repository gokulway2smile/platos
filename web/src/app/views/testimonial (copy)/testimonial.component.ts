import {
    Component,
    OnInit,
    ViewChild,
    TemplateRef
} from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';
import {
    HttpClient
} from '@angular/common/http';

import {
    ActivatedRoute,
    Router
} from "@angular/router";
import {
    DatatableComponent
} from '@swimlane/ngx-datatable';
import {
    AppSettings
} from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss']
})
export class TestimonialComponent implements OnInit {

  constructor( private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) { }

  rowsFilter:any = []; 
  viewdata :any = [];
  AllData:any=[]; 
  is_view:boolean = false;
  is_edit:boolean =false;
  title :any;
  description :any;
  username:any;
  editdata:any;
  image:any;
  error:any;
  fileName:any
  test_image:any;
  is_add:any;
  filePath:any;
  fileData:any;
  myFiles:any =[]
  upload_data:any;

  image_path =AppSettings.TESTIMONIAL_IMAGE;
  ngOnInit() {

  	this.loaddata();
  }

  close()
  {
  	this.is_view = false;
    this.is_edit = false;
    this.is_add  = false;

  }

  edit(id)
  {
  	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_testimonial/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.editdata = data.data; 
        this.title = this.editdata.testi_title
        this.description = this.editdata.testi_description
        this.username = this.editdata.testi_username
        this.test_image = this.editdata.testi_image

        console.log(this.editdata) 
        this.AllData    = [];
        this.is_edit = true;
	})

  }


        getFileDetails (e) {
   //console.log (e.target.files);
   for (var i = 0; i < e.target.files.length; i++) {
     this.myFiles.push(e.target.files[i]);
   }   
 }
  
  

  onSubmit()
     {


     	 this.error = "";   

        
        if (!this.title) {
            this.error = "Please Enter Title";
            return;
        }
        if (!this.description) {
            this.error = "Please Enter Description";
            return;
        }
        if (!this.username) {
            this.error = "Please Enter Description";
            return;
        }
        console.log("Asdsad")
         console.log("Asdsad")
         

        

        if(!this.is_edit)
        {
           const frmData = new FormData();
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
           }
           let data1;
            if (frmData)  
            {
                let temp_data:any;
                this.fileName = null;
                this.filePath = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'testi_image/', frmData).subscribe((temp_data: any) => {
                this.fileData = temp_data.data;
                this.fileName = this.fileData.pic;
                this.filePath = this.fileData.file_path;
                
                 data1 = {
                    "title": this.title,
                    "description": this.description,
                    "username": this.username,
                    "image"   :this.fileName
                } 

                this.addnew(data1);
                });  
            }
            else
                {
                    data1 = {
                    "title": this.title,
                    "description": this.description,
                    "username": this.username                    
                }                   
                }                
                this.addnew(data1);
        }
        else
        {
           const frmData = new FormData();
           let data1;
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
                let temp_data:any;
                this.fileName = null;
                this.filePath = null;        
                this.http.post(AppSettings.API_ENDPOINT + 'testi_image/', frmData).subscribe((temp_data: any) => {
                this.fileData = temp_data.data;
                this.fileName = this.fileData.pic;
                this.filePath = this.fileData.file_path;
                
                 data1 = {                    
                    "title": this.title,
                    "description": this.description,
                    "username": this.username,
                    "image"   :this.fileName
                } 
                
                    this.updateone(data1);
                });  
                }
                else
                {
                     data1 = {
                    "title": this.title,
                    "description": this.description,
                    "username": this.username,
                    "image":this.test_image                   
                }  
                   this.updateone(data1);
                }

        }


        } 
       
       addnew(data1)
    {
        this.spinner.show();  
        this.http.post(AppSettings.API_ENDPOINT + 'getall_testimonial/', {
                "data": data1,
            }).subscribe((data: any) => {
        this.spinner.show();  
                if (data.status == "not_ok") {
                    this.error = data.data;
                    return;
                } else {
                Swal("Success", "Points Added", "success");
                this.loaddata();
                // this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})
        
    }


      updateone(data1)
    {
            this.spinner.show();  
            this.http.put(AppSettings.API_ENDPOINT + 'getall_testimonial/' + this.editdata.testi_id, {
                "data": data1,
            }).subscribe((data: any) => {
            this.spinner.show();  
                if (data.status == "not_ok") {
                    this.error = data.data;
                    return;
                } else {
                Swal("Success", " Testimonial Updated", "success");
                // this.ShowForm = !this.ShowForm;
                this.loaddata();
            }
            }, error => {})

    }

    addAction()
    {
        this.title =""
        this.description =""
        this.username= ""
        this.image=""
        this.is_add =true;

    }

delete(id)
{
    this.spinner.show();        
    
    this.http.delete(AppSettings.API_ENDPOINT + 'getall_testimonial/' +id).subscribe((data: any) => {  
     this.spinner.hide();        
        
       this.loaddata();
    })

}

 viewAction(id)
 {

 	this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_testimonial/' +id).subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.viewdata = data.data;
        console.log(this.viewdata)  
        this.AllData    = [];
        this.is_view = true;
	})


 }

loaddata()
{
  this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_testimonial/').subscribe((data: any) => {  
 	this.spinner.hide();        
 	
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})
}

}
