import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { TestimonialComponent } from './testimonial.component';
import { TestimonialRoutingModule} from './testimonial-routing.module';
import {CommonModule} from  '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [TestimonialComponent],
  imports: [
    CommonModule,
    FormsModule,
    TestimonialRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),NgxDatatableModule,CommonModule
  ]
})
export class TestimonialModule { }
