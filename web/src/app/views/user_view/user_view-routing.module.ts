import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { UserViewComponents } from './user_view.component';

const routes: Routes = [
  {
    path: '',
    component: UserViewComponents,
    data: {
      title: 'User View'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersViewRoutingModule {}
