import {
    Component,
    OnInit,
    ViewChild,
    TemplateRef
} from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';
import {
    HttpClient
} from '@angular/common/http';

import {
    ActivatedRoute,
    Router
} from "@angular/router";
import {
    DatatableComponent
} from '@swimlane/ngx-datatable';
import {
    AppSettings
} from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'user_view.component.html'
})
	


export class UserViewComponents implements OnInit { 

 constructor( private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) {
    }

    rowsFilter:any;
    AllData:any;
    FirstName:any;
    SurName:any;
    EmailAddress:any;
    MobileNo:any;
    Password:any;
    DeliveryPincode:any;
    LeadTime:any;
    DeliveryTime:any
    error:any;
    MinOrder:any;
    maxOrder:any;
    EventType:any;
    GSTCertificate:any;
    FoodCertificate:any;
    Scores:any;
    FileUpload:any;
    showform:boolean= false;


     ngOnInit() {
     	this.loaddata();

     }

     viewAction(id)
     {
         this.router.navigate(['userview/'+id]);
     }

     close()
     {
         this.router.navigate(['users']);
     }

loaddata()
{

	 this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_Userlist/').subscribe((data: any) => {  
 	this.spinner.hide();        

 	
        this.rowsFilter = data.data[0];
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})

}

   
}



