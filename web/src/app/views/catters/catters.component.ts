import {
    Component,
    OnInit,
    ViewChild,
    TemplateRef
} from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';
import {
    HttpClient
} from '@angular/common/http';

import {
    ActivatedRoute,
    Router
} from "@angular/router";
import {
    DatatableComponent
} from '@swimlane/ngx-datatable';
import {
    AppSettings
} from '../../app.setting';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import Swal from 'sweetalert2'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'catters.component.html'
})
	


export class CattersComponents implements OnInit { 

 constructor( private router: Router, private _route: ActivatedRoute,private http: HttpClient,private spinner: NgxSpinnerService) {
    }

    rowsFilter:any;
    AllData:any;
    FirstName:any;
    SurName:any;
    EmailAddress:any;
    MobileNo:any;
    Password:any;
    DeliveryPincode:any;
    LeadTime:any;
    DeliveryTime_from:any;
    DeliveryTime_to:any
    error:any;
    myFiles:any=[];
    MinOrder:any;
    maxOrder:any;
    EventType:any;
    GSTCertificate:any;
    FoodCertificate:any;
    Scores:any;
    meridian: boolean = true;
    is_edit:boolean = false;
    FileUpload:any;
    showform:boolean= false;


     ngOnInit() {
     	this.loaddata();

     }

     close()
     {
       this.showform = false;
     }


 getFileDetails (e) {
   //console.log (e.target.files);
   for (var i = 0; i < e.target.files.length; i++) {
     this.myFiles.push(e.target.files[i]);
   }   
 }
  
  


     addAction()
     {
       this.showform =true;
        this.FirstName="";
        this.SurName="";
        this.EmailAddress="";
        this.MobileNo="";
        this.Password="";
        this.DeliveryPincode="";
        this.LeadTime="";
        this.DeliveryTime_from="";
        this.DeliveryTime_to="";
        this.MinOrder="";
        this.maxOrder="";
        this.EventType="";
        this.GSTCertificate="";
        this.FoodCertificate="";
        this.Scores="";
        this.FileUpload="";

     }

     onSubmit()
     {
        
         this.error = "";          
        if (!this.FirstName) {
            this.error = "Please Enter FirstName";
            return;
        }
        if (!this.SurName) {
            this.error = "Please Enter SurName";
            return;
        }
        if (!this.MobileNo) {
            this.error = "Please Enter MobileNo";
            return;
        }
        if (!this.Password) {
            this.error = "Please Enter Password";
            return;
        }
        if (!this.DeliveryPincode) {
            this.error = "Please Enter Delivery Pincode";
            return;
        }
        if (!this.LeadTime) {
            this.error = "Please Enter Lead Time";
            return;
        }
        if (!this.DeliveryTime_from) {
            this.error = "Please Enter Delivery From Time";
            return;
        }
        if (!this.DeliveryTime_to) {
            this.error = "Please Enter Delivery To Time";
            return;
        }

        if(!this.is_edit)
        {

        const frmData = new FormData();
           if(this.myFiles.length>0)
           {
           for (var i = 0; i < this.myFiles.length; i++) {
             frmData.append("pic", this.myFiles[i]);
           }
           }

           console.log(this)
            let data = {
            "FirstName" : this.FirstName,
            "SurName": this.SurName,
            "EmailAddress": this.EmailAddress,
            "MobileNo": this.MobileNo,
            "Password" : this.Password,
            "DeliveryPincode":this.DeliveryPincode,
            "LeadTime": this.LeadTime,
            "DeliveryTime" : this.DeliveryTime_from + this.DeliveryTime_to,            
            "MinOrder" : this.MinOrder,
            "maxOrder" : this.maxOrder,
            "EventType" : this.EventType,
            "GSTCertificate" : this.GSTCertificate,
            "FoodCertificate" :this.FoodCertificate,
            "Scores" :this.Scores,
            "FileUpload":this.FileUpload
            }


           console.log(data)
           // this.http.post(AppSettings.API_ENDPOINT + 'Catter_NewAdmin/', {
           //      "data": data,
           //  }).subscribe((data: any) => {

           //      console.log(data);
           //      if (data.status == "not_ok") {
           //          this.error = data.data;
           //          return;
           //      } else {
                
           //      Swal("Success", "Caterer Added", "success");                
           //      this.showform = !this.showform;
           //      this.loaddata();
           //  }
            
           //  }, error => {})

       }




        }   

     
loaddata()
{

	 this.spinner.show();        
	
	this.http.get(AppSettings.API_ENDPOINT + 'getall_catterlist/').subscribe((data: any) => {  
 	this.spinner.hide();        

 	
        this.rowsFilter = data.data;
        console.log(this.rowsFilter)  
        this.AllData    = [];
	})

}

   
}



