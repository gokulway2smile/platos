import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule,PreloadAllModules } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { AppSettings } from './app.setting';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';


import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { TokenInterceptor } from './token.interceptor';

import { CommonModule } from '@angular/common';

import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { NgxSpinnerModule } from 'ngx-spinner';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'prefix',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
    {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },

      {
        path: 'catters',
        loadChildren: './views/catters/catters.module#CattersModule'
      },
      
      {
        path: 'users',
        loadChildren: './views/users/users.module#UsersModule'
      },
      {
        path: 'userview/:id',
        loadChildren: './views/user_view/user_view.module#UsersViewModule'
      },
      
      
       {
        path: 'testimonial',
        loadChildren: './views/testimonial/testimonial.module#TestimonialModule'
      },
      {
        path: 'base',
        loadChildren: './views/base/base.module#BaseModule'
      }     
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{ preloadingStrategy: PreloadAllModules,onSameUrlNavigation: "reload" ,enableTracing: true }),
  NgMultiSelectDropDownModule.forRoot(),
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  NgxSpinnerModule,
  BrowserModule,
  HttpClientModule,
  NgbModule,
  
  JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        whitelistedDomains: [AppSettings.DOMAIN]
      }
    })    

  ],

  providers: [

     AuthGuard,
     AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },


    HttpClient
  ],

  exports: [ RouterModule ]
})
export class AppRoutingModule {}
