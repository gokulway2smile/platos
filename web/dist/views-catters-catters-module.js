(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-catters-catters-module"],{

/***/ "./src/app/views/catters/catters-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/catters/catters-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: CattersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CattersRoutingModule", function() { return CattersRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _catters_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./catters.component */ "./src/app/views/catters/catters.component.ts");




var routes = [
    {
        path: '',
        component: _catters_component__WEBPACK_IMPORTED_MODULE_3__["CattersComponents"],
        data: {
            title: 'Catters'
        }
    }
];
var CattersRoutingModule = /** @class */ (function () {
    function CattersRoutingModule() {
    }
    CattersRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CattersRoutingModule);
    return CattersRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/catters/catters.component.html":
/*!******************************************************!*\
  !*** ./src/app/views/catters/catters.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\" *ngIf=\"showform\">\n\n <div class=\"row\" style=\"padding: 16px\">\n      <div class=\"col-sm-12\">\n        <a  (click)=\"close()\" mdbRippleRadius style=\"cursor: pointer;\" ><i class=\"fa fa-chevron-left\"></i> BACK</a>          \n      </div>\n    </div>\n\n    <div class=\"row\">\n    <div class=\"col-lg-12\">\n       <div class=\"auth-box card\">\n      <div class=\"card-block\">\n      <div class=\"general-info form-material\" style=\"padding: 28px\">\n\n  <form  (ngSubmit)=\"onSubmit()\">\n              <div class=\"container1\" >             \n              <div class=\"form-group\">                \n                    <input type=\"text\" placeholder=\"First Name\" [(ngModel)]= \"FirstName\" name=\"firstname\" class=\"form-control\" />\n                    </div>                  \n                   \n                    <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Sur Name\" [(ngModel)]= \"SurName\"name=\"surname\" class=\"form-control\" />                      \n                  </div>\n\n                    <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Email Address\" [(ngModel)]= \"EmailAddress\"name=\"email\" class=\"form-control\" />                      \n                  </div>\n\n\n                    <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Mobile No\"[(ngModel)]= \"MobileNo\" name=\"mobile\" class=\"form-control\" />                      \n                  </div>\n\n                  <div class=\"form-group\">\n                        <input type=\"password\" placeholder=\"Password\" [(ngModel)]= \"Password\"name=\"password\" class=\"form-control\" />                      \n                  </div>\n\n\n                  <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Delivery Pincode\" [(ngModel)]= \"DeliveryPincode\"name=\"pincode\" class=\"form-control\" />                      \n                  </div>\n\n\n                  <div class=\"form-group\">\n                    <div class=\"row\">\n                      <div class=\"col-md-6\">\n                        <label>Delivery From Time</label>\n                        <ngb-timepicker name=\"delivery_time_from\"[(ngModel)]=\"DeliveryTime_from\" [meridian]=\"meridian\"></ngb-timepicker>\n                        </div>\n                        <div class=\"col-md-6\">\n                        <label>Delivery To Time</label>\n                        <ngb-timepicker name=\"delivery_time_to\"[(ngModel)]=\"DeliveryTime_to\" [meridian]=\"meridian\"></ngb-timepicker>\n                        </div>\n                  </div>\n                </div>\n\n\n                  <div class=\"form-group\">\n                        <input type=\"number\" placeholder=\"Min Order\" [(ngModel)]= \"MinOrder\"name=\"min_order\" class=\"form-control\" />                      \n                  </div>\n\n                  <div class=\"form-group\">\n                        <input type=\"number\" placeholder=\"Max Order\" [(ngModel)]= \"maxOrder\"name=\"max_order\" class=\"form-control\" />                      \n                  </div>\n\n                  <div class=\"form-group\">\n                         <input type=\"number\" placeholder=\"Lead Time (in days)\" [(ngModel)]= \"LeadTime\"name=\"LeadTime\" class=\"form-control\" />\n                  </div>\n\n                  <div class=\"form-group\">\n                        <select class=\"form-control\" name=\"EventType\"> \n                          <option>--Type 1 --</option>\n                          <option>--Type 2 --</option>\n                          <option>--Type 3 --</option>\n                          <option>--Type 4 --</option>\n\n                        </select>                    \n                  </div>\n\n                  <div class=\"form-group\">\n                    <label>GST Certificate</label>\n                        <input type=\"file\" placeholder=\"GST Certificate\" (change)=\"getFileDetails($event)\" [(ngModel)]= \"GSTCertificate\"name=\"gst\" class=\"form-control\" />                      \n                  </div>\n\n                   <div class=\"form-group\">\n                    <label>Food Certificate</label>\n                        <input type=\"file\" placeholder=\"Food Certificate\" (change)=\"getFileDetails($event)\"[(ngModel)]= \"FoodCertificate\"name=\"food\" class=\"form-control\" />                      \n                  </div>\n\n                  <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Score\" name=\"scores\" [(ngModel)]= \"Scores\"class=\"form-control\" />                      \n                  </div>\n\n                  <div class=\"form-group\">\n                      <label>File Upload</label>\n                        <input type=\"file\" placeholder=\"File Upload\" (change)=\"getFileDetails($event)\"[(ngModel)]= \"FileUpload\"name=\"file\" class=\"form-control\" />                      \n                  </div>\n\n                <div class=\"col-md-6\">\n                  <div class=\"material-group\">                  \n                    <div class=\"form-group form-primary\">\n                      <!-- <ng-multiselect-dropdown\n                      [placeholder]=\"'Select Package'\"\n                      [data]=\"dropdownList\"\n                      [(ngModel)]=\"selectedItems1\"\n                      [settings]=\"dropdownSettings1\"\n                      name=\"selectedItems1\"\n                        >\n                    </ng-multiselect-dropdown> -->\n                    </div>\n                  </div>\n                </div>\n\n                         \n            <div class=\"error\">\n         {{error}} \n         </div>\n           <div class=\"row\">                \n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"button\" style=\"margin-left: 10px\" (click)=\"close()\"> Close</button>\n                  \n\n\n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"submit\" style=\"margin-left: 10px\"  >{{map_id ? 'Update':'Save'}}</button>\n                 </div>\n              </div>\n  </form>\n \n\n</div>\n\n\n\n\n</div>\n</div>\n\n</div>\n</div>\n</div>\n\n\n\n  \n<div class=\"card\">\n  <div class=\"col-sm-12 card-header\">\n    <div class=\"row\">\n      <div class=\"col-sm-12\">\n        <label class=\"dt-cust-search float-right\">\n          <button  class=\"btn btn-primary m-b-0 waves-light\" mdbRippleRadius type=\"button\" (click)=\"addAction()\"> Add</button>\n        </label>\n\n       \n\n        <label class=\"dt-cust-search float-right\">\n          <img src=\"assets/images/search.png\" style=\"width: 21px;float: left;margin-top: 8px;\">\n          <div class=\"form-group\" style=\"float: left\">\n            <!-- <label>Search: </label> -->\n\n            <input type='text' class=\"form-control input-sm m-l-10\" placeholder='Search' (keyup)='updateFilter($event)' style=\"background: none;border: none;border-bottom: 1px solid #d6d2d2;\" />\n          </div>\n        </label>\n      </div>\n    </div>\n  </div>\n\n    <!-- <app-card [title]=\"'Map Management'\"> -->\n    <div class=\"card-body\">\n      <!--<span class=\"m-b-20\">use class <code>data-table</code> with ngx-datatable</span><br><br>-->\n      <ngx-datatable\n        class=\"data-table\"\n        [rows]=\"rowsFilter\"\n        [columnMode]=\"'force'\"\n        [headerHeight]=\"50\"\n        [footerHeight]=\"50\"\n        [scrollbarH]=\"true\"\n        [rowHeight]=\"'auto'\"\n        [limit]=\"10\">\n\n        <ngx-datatable-column [width]=\"80\" name=\"S.No\">          \n         <ng-template let-rowIndex=\"rowIndex\" let-row=\"row\"  let-expanded=\"expanded\" ngx-datatable-cell-template>\n                {{rowIndex+1}}\n         </ng-template>       \n        </ngx-datatable-column>\n     \n      <ngx-datatable-column name=\"First Name\" prop=\"data\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.catt_first_name}}\n        </ng-template>\n\n      </ngx-datatable-column>          \n      \n       <ngx-datatable-column name=\"Email Address\" prop=\"v1\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.catt_email_address}}\n        </ng-template>\n\n      </ngx-datatable-column> \n       <ngx-datatable-column name=\"Mobile No\" prop=\"v2\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.catt_mobile_no}}\n        </ng-template>\n\n      </ngx-datatable-column> \n\n\n      <ngx-datatable-column name=\"Event Type\" prop=\"v2\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.catt_event_type}}\n        </ng-template>\n\n      </ngx-datatable-column> \n\n      <ngx-datatable-column name=\"Pincode\" prop=\"v2\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.catt_delivery_pincode}}\n        </ng-template>\n\n      </ngx-datatable-column> \n\n\n\n      <ngx-datatable-column  name=\"Actions\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n           \n           <a  style=\"cursor: pointer;\"  title=\"View\" (click)=\"viewAction(row.map_id)\"><img class=\"data_img\" src=\"assets/images/view.png\"></a>\n          <a  style=\"cursor: pointer;\" title=\"Edit\" (click)=\"edit(row.map_id)\"><img class=\"data_img\" src=\"assets/images/edit.png\"></a>\n          <a  style=\"cursor: pointer;\" title=\"Delete\"  (click)=\"delete(row.map_id)\"><img class=\"data_img\" src=\"assets/images/delete.png\"></a>\n          \n\n        \n        </ng-template>\n      </ngx-datatable-column>\n      </ngx-datatable>\n    </div>\n    <!-- </app-card> -->\n  </div>\n\n"

/***/ }),

/***/ "./src/app/views/catters/catters.component.ts":
/*!****************************************************!*\
  !*** ./src/app/views/catters/catters.component.ts ***!
  \****************************************************/
/*! exports provided: CattersComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CattersComponents", function() { return CattersComponents; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_setting__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.setting */ "./src/app/app.setting.ts");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");






var CattersComponents = /** @class */ (function () {
    function CattersComponents(router, _route, http, spinner) {
        this.router = router;
        this._route = _route;
        this.http = http;
        this.spinner = spinner;
        this.myFiles = [];
        this.meridian = true;
        this.is_edit = false;
        this.showform = false;
    }
    CattersComponents.prototype.ngOnInit = function () {
        this.loaddata();
    };
    CattersComponents.prototype.close = function () {
        this.showform = false;
    };
    CattersComponents.prototype.getFileDetails = function (e) {
        //console.log (e.target.files);
        for (var i = 0; i < e.target.files.length; i++) {
            this.myFiles.push(e.target.files[i]);
        }
    };
    CattersComponents.prototype.addAction = function () {
        this.showform = true;
        this.FirstName = "";
        this.SurName = "";
        this.EmailAddress = "";
        this.MobileNo = "";
        this.Password = "";
        this.DeliveryPincode = "";
        this.LeadTime = "";
        this.DeliveryTime_from = "";
        this.DeliveryTime_to = "";
        this.MinOrder = "";
        this.maxOrder = "";
        this.EventType = "";
        this.GSTCertificate = "";
        this.FoodCertificate = "";
        this.Scores = "";
        this.FileUpload = "";
    };
    CattersComponents.prototype.onSubmit = function () {
        this.error = "";
        if (!this.FirstName) {
            this.error = "Please Enter FirstName";
            return;
        }
        if (!this.SurName) {
            this.error = "Please Enter SurName";
            return;
        }
        if (!this.MobileNo) {
            this.error = "Please Enter MobileNo";
            return;
        }
        if (!this.Password) {
            this.error = "Please Enter Password";
            return;
        }
        if (!this.DeliveryPincode) {
            this.error = "Please Enter Delivery Pincode";
            return;
        }
        if (!this.LeadTime) {
            this.error = "Please Enter Lead Time";
            return;
        }
        if (!this.DeliveryTime_from) {
            this.error = "Please Enter Delivery From Time";
            return;
        }
        if (!this.DeliveryTime_to) {
            this.error = "Please Enter Delivery To Time";
            return;
        }
        if (!this.is_edit) {
            var frmData = new FormData();
            if (this.myFiles.length > 0) {
                for (var i = 0; i < this.myFiles.length; i++) {
                    frmData.append("pic", this.myFiles[i]);
                }
            }
            console.log(this);
            var data = {
                "FirstName": this.FirstName,
                "SurName": this.SurName,
                "EmailAddress": this.EmailAddress,
                "MobileNo": this.MobileNo,
                "Password": this.Password,
                "DeliveryPincode": this.DeliveryPincode,
                "LeadTime": this.LeadTime,
                "DeliveryTime": this.DeliveryTime_from + this.DeliveryTime_to,
                "MinOrder": this.MinOrder,
                "maxOrder": this.maxOrder,
                "EventType": this.EventType,
                "GSTCertificate": this.GSTCertificate,
                "FoodCertificate": this.FoodCertificate,
                "Scores": this.Scores,
                "FileUpload": this.FileUpload
            };
            console.log(data);
            // this.http.post(AppSettings.API_ENDPOINT + 'Catter_NewAdmin/', {
            //      "data": data,
            //  }).subscribe((data: any) => {
            //      console.log(data);
            //      if (data.status == "not_ok") {
            //          this.error = data.data;
            //          return;
            //      } else {
            //      Swal("Success", "Caterer Added", "success");                
            //      this.showform = !this.showform;
            //      this.loaddata();
            //  }
            //  }, error => {})
        }
    };
    CattersComponents.prototype.loaddata = function () {
        var _this = this;
        this.spinner.show();
        this.http.get(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_catterlist/').subscribe(function (data) {
            _this.spinner.hide();
            _this.rowsFilter = data.data;
            console.log(_this.rowsFilter);
            _this.AllData = [];
        });
    };
    CattersComponents = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./catters.component.html */ "./src/app/views/catters/catters.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]])
    ], CattersComponents);
    return CattersComponents;
}());



/***/ }),

/***/ "./src/app/views/catters/catters.module.ts":
/*!*************************************************!*\
  !*** ./src/app/views/catters/catters.module.ts ***!
  \*************************************************/
/*! exports provided: CattersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CattersModule", function() { return CattersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _catters_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./catters.component */ "./src/app/views/catters/catters.component.ts");
/* harmony import */ var _catters_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./catters-routing.module */ "./src/app/views/catters/catters-routing.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");











var CattersModule = /** @class */ (function () {
    function CattersModule() {
    }
    CattersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _catters_routing_module__WEBPACK_IMPORTED_MODULE_7__["CattersRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__["NgbModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(), _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__["NgxDatatableModule"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"]
            ],
            declarations: [_catters_component__WEBPACK_IMPORTED_MODULE_6__["CattersComponents"]]
        })
    ], CattersModule);
    return CattersModule;
}());



/***/ })

}]);
//# sourceMappingURL=views-catters-catters-module.js.map