(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-testimonial-testimonial-module"],{

/***/ "./src/app/views/testimonial/testimonial-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/testimonial/testimonial-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: TestimonialRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestimonialRoutingModule", function() { return TestimonialRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _testimonial_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./testimonial.component */ "./src/app/views/testimonial/testimonial.component.ts");




var routes = [
    {
        path: '',
        component: _testimonial_component__WEBPACK_IMPORTED_MODULE_3__["TestimonialComponent"],
        data: {
            title: 'Testimonial'
        },
    }
];
var TestimonialRoutingModule = /** @class */ (function () {
    function TestimonialRoutingModule() {
    }
    TestimonialRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TestimonialRoutingModule);
    return TestimonialRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/testimonial/testimonial.component.html":
/*!**************************************************************!*\
  !*** ./src/app/views/testimonial/testimonial.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- View Testimonial-->\n\n<div class=\"card\" *ngIf=\"is_view\">\n\n <div class=\"row\" style=\"padding: 16px\">\n      <div class=\"col-sm-12\">\n        <a  (click)=\"close()\" mdbRippleRadius style=\"cursor: pointer;\" ><i class=\"fa fa-chevron-left\"></i> BACK</a>          \n      </div>\n    </div>\n\n    <div class=\"row\">\n    <div class=\"col-lg-12\">\n       <div class=\"auth-box card\">\n      <div class=\"card-block\">\n      <div class=\"general-info form-material\" style=\"padding: 28px\">\n\n  <form  (ngSubmit)=\"onSubmit()\">\n              <div class=\"container1\" >             \n              <div class=\"form-group\">   \n              \t<label>Title</label>             \n                    <input type=\"text\"  readonly=\"\" [(ngModel)]= \"viewdata.testi_title\" name=\"firstname\" class=\"form-control\" />\n                    </div>                  \n                   \n                    <div class=\"form-group\">\n\n                    \t<label>Description</label>             \n                        <textarea readonly=\"\"[(ngModel)]= \"viewdata.testi_description\"name=\"surname\" class=\"form-control\"></textarea>                     \n                  </div>\n\n                    <div class=\"form-group\">\n                    \t<label>Username</label>             \n                        <input type=\"text\"  readonly=\"\"[(ngModel)]= \"viewdata.testi_username\"name=\"email\" class=\"form-control\" />                      \n                  </div>\n                 \n                  <div class=\"form-group\">\n                  \t    <img src=\"{{image_path}}{{viewdata.testi_image}}\">                   \n                  </div>\n\n           <div class=\"row\">                \n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"button\" style=\"margin-left: 10px\" (click)=\"close()\"> Close</button>\n                 </div>\n              </div>\n  </form>\n</div>\n</div>\n</div>\n\n</div>\n</div>\n</div>\n\n\n<div class=\"card\" *ngIf=\"is_edit\">\n\n <div class=\"row\" style=\"padding: 16px\">\n      <div class=\"col-sm-12\">\n        <a  (click)=\"close()\" mdbRippleRadius style=\"cursor: pointer;\" ><i class=\"fa fa-chevron-left\"></i> BACK</a>          \n      </div>\n    </div>\n\n    <div class=\"row\">\n    <div class=\"col-lg-12\">\n       <div class=\"auth-box card\">\n      <div class=\"card-block\">\n      <div class=\"general-info form-material\" style=\"padding: 28px\">\n  \n      <form #myForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n              <div class=\"container1\" >                         \n              <div class=\"form-group\">   \n                <label>Title</label>             \n                    <input type=\"text\"  [(ngModel)]= \"title\" name=\"sdas\" class=\"form-control\" />\n                    </div>                  \n                   \n                    <div class=\"form-group\">\n\n                      <label>Description</label>             \n                        <textarea [(ngModel)]= \"description\" name=\"asssad\"class=\"form-control\"></textarea>                     \n                  </div>\n\n                    <div class=\"form-group\">\n                      <label>Username</label>             \n                        <input type=\"text\"  [(ngModel)]= \"username\" name=\"asxz\"class=\"form-control\" />                      \n                  </div>\n                 \n                  <div class=\"form-group\">\n                    <label>Image</label>    \n                        <input type=\"file\" (change)=\"getFileDetails($event)\" [(ngModel)]= \"test_image\"accept=\".jpg,.png,.jpeg,.PNG\" class=\"form-control\" />                  \n                   <img  style=\"height: 179px;width: 254px;\" src=\"{{image_path}}{{test_image}}\" *ngIf=is_edit>\n                  </div>\n\n                  <div class=\"error\">\n         {{error}} \n         </div>\n           <div class=\"row\">                \n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"button\" style=\"margin-left: 10px\" (click)=\"close()\"> Close</button>\n\n                    <input type=\"hidden\" name=\"\">\n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"submit\" style=\"margin-left: 10px\"  >{{editdata.testi_id ? 'Update':'Save'}}</button>\n\n                 </div>\n              </div>\n  </form>\n</div>\n</div>\n</div>\n\n</div>\n</div>\n</div>\n\n\n\n<div class=\"card\" *ngIf=\"is_add\">\n\n <div class=\"row\" style=\"padding: 16px\">\n      <div class=\"col-sm-12\">\n        <a  (click)=\"close()\" mdbRippleRadius style=\"cursor: pointer;\" ><i class=\"fa fa-chevron-left\"></i> BACK</a>          \n      </div>\n    </div>\n\n    <div class=\"row\">\n    <div class=\"col-lg-12\">\n       <div class=\"auth-box card\">\n      <div class=\"card-block\">\n      <div class=\"general-info form-material\" style=\"padding: 28px\">\n\n  <form #myForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n              <div class=\"container1\" >             \n              <div class=\"form-group\">                \n                    <input type=\"text\" placeholder=\"Title\" [(ngModel)]= \"title\" name=\"ti\"  class=\"form-control\" />\n                    </div>                  \n                   \n                    <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Description\" name=\"de\" [(ngModel)]= \"description\" class=\"form-control\" />                      \n                  </div>\n\n                    <div class=\"form-group\">\n                        <input type=\"text\" placeholder=\"Username\" [(ngModel)]= \"username\" name=\"us\"class=\"form-control\" />                      \n                  </div>\n\n                   <div class=\"form-group\">\n                        <input type=\"file\" placeholder=\"Testi_image\" (change)=\"getFileDetails($event)\" name=\"te\"accept=\".jpg,.png,.jpeg,.PNG\" class=\"form-control\" />                      \n                  </div>\n\n                \n                         \n            <div class=\"error\">\n         {{error}} \n         </div>\n           <div class=\"row\">                \n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"button\" style=\"margin-left: 10px\" (click)=\"close()\"> Close</button>\n                  \n\n\n                    <button class=\"btn btn-md btn-primary waves-light pull-right\" mdbRippleRadius type=\"submit\" style=\"margin-left: 10px\"  >{{testi_id ? 'Update':'Save'}}</button>\n                 </div>\n              </div>\n  </form>\n \n\n</div>\n\n\n\n\n</div>\n</div>\n\n</div>\n</div>\n</div>\n\n\n\n  \n<div class=\"card\">\n  <div class=\"col-sm-12 card-header\">\n    <div class=\"row\">\n      <div class=\"col-sm-12\">\n        <label class=\"dt-cust-search float-right\">\n          <button  class=\"btn btn-primary m-b-0 waves-light\" mdbRippleRadius type=\"button\" (click)=\"addAction()\"> Add</button>\n        </label>\n\n       \n\n        <label class=\"dt-cust-search float-right\">\n          <img src=\"assets/images/search.png\" style=\"width: 21px;float: left;margin-top: 8px;\">\n          <div class=\"form-group\" style=\"float: left\">\n            <!-- <label>Search: </label> -->\n\n            <input type='text' class=\"form-control input-sm m-l-10\" placeholder='Search' (keyup)='updateFilter($event)' style=\"background: none;border: none;border-bottom: 1px solid #d6d2d2;\" />\n          </div>\n        </label>\n      </div>\n    </div>\n  </div>\n\n    <!-- <app-card [title]=\"'Map Management'\"> -->\n    <div class=\"card-body\">\n      <!--<span class=\"m-b-20\">use class <code>data-table</code> with ngx-datatable</span><br><br>-->\n      <ngx-datatable\n        class=\"data-table\"\n        [rows]=\"rowsFilter\"\n        [columnMode]=\"'force'\"\n        [headerHeight]=\"50\"\n        [footerHeight]=\"50\"\n        [scrollbarH]=\"true\"\n        [rowHeight]=\"'auto'\"\n        [limit]=\"10\">\n\n        <ngx-datatable-column [width]=\"80\" name=\"S.No\">          \n         <ng-template let-rowIndex=\"rowIndex\" let-row=\"row\"  let-expanded=\"expanded\" ngx-datatable-cell-template>\n                {{rowIndex+1}}\n         </ng-template>       \n        </ngx-datatable-column>\n     \n      <ngx-datatable-column name=\"Title\" prop=\"data\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.testi_title}}\n        </ng-template>\n\n      </ngx-datatable-column>          \n      \n       <ngx-datatable-column name=\"Username\" prop=\"v1\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.testi_username}}\n        </ng-template>\n\n      </ngx-datatable-column> \n       <ngx-datatable-column name=\"Created date\" prop=\"v2\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.created_at}}\n        </ng-template>\n\n      </ngx-datatable-column> \n\n\n      <ngx-datatable-column name=\"Modified Date\" prop=\"v2\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n          {{row.updated_at}}\n        </ng-template>\n\n      </ngx-datatable-column> \n\n   \n      <ngx-datatable-column  name=\"Actions\">\n        <ng-template let-row=\"row\" let-expanded=\"expanded\" ngx-datatable-cell-template>\n           \n           <a  style=\"cursor: pointer;\"  title=\"View\" (click)=\"viewAction(row.testi_id)\"><img class=\"data_img\" src=\"assets/images/view.png\"></a>\n          <a  style=\"cursor: pointer;\" title=\"Edit\" (click)=\"edit(row.testi_id)\"><img class=\"data_img\" src=\"assets/images/edit.png\"></a>\n          <a  style=\"cursor: pointer;\" title=\"Delete\"  (click)=\"delete(row.testi_id)\"><img class=\"data_img\" src=\"assets/images/delete.png\"></a>\n          \n\n        \n        </ng-template>\n      </ngx-datatable-column>\n      </ngx-datatable>\n    </div>\n    <!-- </app-card> -->\n  </div>\n\n"

/***/ }),

/***/ "./src/app/views/testimonial/testimonial.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/views/testimonial/testimonial.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3Rlc3RpbW9uaWFsL3Rlc3RpbW9uaWFsLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/testimonial/testimonial.component.ts":
/*!************************************************************!*\
  !*** ./src/app/views/testimonial/testimonial.component.ts ***!
  \************************************************************/
/*! exports provided: TestimonialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestimonialComponent", function() { return TestimonialComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_setting__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.setting */ "./src/app/app.setting.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");







var TestimonialComponent = /** @class */ (function () {
    function TestimonialComponent(router, _route, http, spinner) {
        this.router = router;
        this._route = _route;
        this.http = http;
        this.spinner = spinner;
        this.rowsFilter = [];
        this.viewdata = [];
        this.AllData = [];
        this.is_view = false;
        this.is_edit = false;
        this.myFiles = [];
        this.image_path = _app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].TESTIMONIAL_IMAGE;
    }
    TestimonialComponent.prototype.ngOnInit = function () {
        this.loaddata();
    };
    TestimonialComponent.prototype.close = function () {
        this.is_view = false;
        this.is_edit = false;
        this.is_add = false;
    };
    TestimonialComponent.prototype.edit = function (id) {
        var _this = this;
        this.spinner.show();
        this.http.get(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_testimonial/' + id).subscribe(function (data) {
            _this.spinner.hide();
            _this.editdata = data.data;
            _this.title = _this.editdata.testi_title;
            _this.description = _this.editdata.testi_description;
            _this.username = _this.editdata.testi_username;
            _this.test_image = _this.editdata.testi_image;
            console.log(_this.editdata);
            _this.AllData = [];
            _this.is_edit = true;
        });
    };
    TestimonialComponent.prototype.getFileDetails = function (e) {
        //console.log (e.target.files);
        for (var i = 0; i < e.target.files.length; i++) {
            this.myFiles.push(e.target.files[i]);
        }
    };
    TestimonialComponent.prototype.onSubmit = function () {
        var _this = this;
        this.error = "";
        if (!this.title) {
            this.error = "Please Enter Title";
            return;
        }
        if (!this.description) {
            this.error = "Please Enter Description";
            return;
        }
        if (!this.username) {
            this.error = "Please Enter Description";
            return;
        }
        if (!this.is_edit) {
            var frmData = new FormData();
            if (this.myFiles.length > 0) {
                for (var i = 0; i < this.myFiles.length; i++) {
                    frmData.append("pic", this.myFiles[i]);
                }
            }
            var data1_1;
            if (frmData) {
                var temp_data = void 0;
                this.fileName = null;
                this.filePath = null;
                this.http.post(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'testi_image/', frmData).subscribe(function (temp_data) {
                    _this.fileData = temp_data.data;
                    _this.fileName = _this.fileData.pic;
                    _this.filePath = _this.fileData.file_path;
                    data1_1 = {
                        "title": _this.title,
                        "description": _this.description,
                        "username": _this.username,
                        "image": _this.fileName
                    };
                    _this.addnew(data1_1);
                });
            }
            else {
                data1_1 = {
                    "title": this.title,
                    "description": this.description,
                    "username": this.username
                };
            }
            this.addnew(data1_1);
        }
        else {
            var frmData = new FormData();
            var data1_2;
            if (this.myFiles.length > 0) {
                for (var i = 0; i < this.myFiles.length; i++) {
                    frmData.append("pic", this.myFiles[i]);
                }
                var temp_data = void 0;
                this.fileName = null;
                this.filePath = null;
                this.http.post(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'testi_image/', frmData).subscribe(function (temp_data) {
                    _this.fileData = temp_data.data;
                    _this.fileName = _this.fileData.pic;
                    _this.filePath = _this.fileData.file_path;
                    data1_2 = {
                        "title": _this.title,
                        "description": _this.description,
                        "username": _this.username,
                        "image": _this.fileName
                    };
                    _this.updateone(data1_2);
                });
            }
            else {
                data1_2 = {
                    "title": this.title,
                    "description": this.description,
                    "username": this.username,
                    "image": this.test_image
                };
                this.updateone(data1_2);
            }
        }
    };
    TestimonialComponent.prototype.addnew = function (data1) {
        var _this = this;
        this.spinner.show();
        this.http.post(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_testimonial/', {
            "data": data1,
        }).subscribe(function (data) {
            _this.spinner.show();
            if (data.status == "not_ok") {
                _this.error = data.data;
                return;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()("Success", "Points Added", "success");
                _this.loaddata();
                // this.ShowForm = !this.ShowForm;
                _this.loaddata();
            }
        }, function (error) { });
    };
    TestimonialComponent.prototype.updateone = function (data1) {
        var _this = this;
        this.spinner.show();
        this.http.put(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_testimonial/' + this.editdata.testi_id, {
            "data": data1,
        }).subscribe(function (data) {
            _this.spinner.show();
            if (data.status == "not_ok") {
                _this.error = data.data;
                return;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()("Success", " Testimonial Updated", "success");
                // this.ShowForm = !this.ShowForm;
                _this.loaddata();
            }
        }, function (error) { });
    };
    TestimonialComponent.prototype.addAction = function () {
        this.title = "";
        this.description = "";
        this.username = "";
        this.image = "";
        this.is_add = true;
    };
    TestimonialComponent.prototype.delete = function (id) {
        var _this = this;
        this.spinner.show();
        this.http.delete(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_testimonial/' + id).subscribe(function (data) {
            _this.spinner.hide();
            _this.loaddata();
        });
    };
    TestimonialComponent.prototype.viewAction = function (id) {
        var _this = this;
        this.spinner.show();
        this.http.get(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_testimonial/' + id).subscribe(function (data) {
            _this.spinner.hide();
            _this.viewdata = data.data;
            console.log(_this.viewdata);
            _this.AllData = [];
            _this.is_view = true;
        });
    };
    TestimonialComponent.prototype.loaddata = function () {
        var _this = this;
        this.spinner.show();
        this.http.get(_app_setting__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].API_ENDPOINT + 'getall_testimonial/').subscribe(function (data) {
            _this.spinner.hide();
            _this.rowsFilter = data.data;
            console.log(_this.rowsFilter);
            _this.AllData = [];
        });
    };
    TestimonialComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-testimonial',
            template: __webpack_require__(/*! ./testimonial.component.html */ "./src/app/views/testimonial/testimonial.component.html"),
            styles: [__webpack_require__(/*! ./testimonial.component.scss */ "./src/app/views/testimonial/testimonial.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], ngx_spinner__WEBPACK_IMPORTED_MODULE_6__["NgxSpinnerService"]])
    ], TestimonialComponent);
    return TestimonialComponent;
}());



/***/ }),

/***/ "./src/app/views/testimonial/testimonial.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/testimonial/testimonial.module.ts ***!
  \*********************************************************/
/*! exports provided: TestimonialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestimonialModule", function() { return TestimonialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _testimonial_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./testimonial.component */ "./src/app/views/testimonial/testimonial.component.ts");
/* harmony import */ var _testimonial_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./testimonial-routing.module */ "./src/app/views/testimonial/testimonial-routing.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__);










var TestimonialModule = /** @class */ (function () {
    function TestimonialModule() {
    }
    TestimonialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_testimonial_component__WEBPACK_IMPORTED_MODULE_6__["TestimonialComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _testimonial_routing_module__WEBPACK_IMPORTED_MODULE_7__["TestimonialRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(), _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_9__["NgxDatatableModule"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"]
            ]
        })
    ], TestimonialModule);
    return TestimonialModule;
}());



/***/ })

}]);
//# sourceMappingURL=views-testimonial-testimonial-module.js.map